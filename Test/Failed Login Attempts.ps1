﻿#From Public CLC Repo
#Need for LT remote monitor
#"%windir%\System32\WindowsPowerShell\v1.0\powershell.exe" -noprofile -command "& { 
function Check-FailedLogins {
    Param(
        [int] $checkThreshold = 5,
        [int] $minutes = 5,
        [System.Collections.Generic.List[string]]$ignoreIPs,
#        [System.Collections.Generic.List[string]]$ignoreNames,
        [Switch] $ignoreAuditpol,
        [Switch] $showErrors
    )
#    $checkThreshold = -1;
#    $minutes = 5000;
#    [System.Collections.Generic.List[string]]$ignoreIPs = New-Object 'System.Collections.Generic.List[string]';
#    [System.Collections.Generic.List[string]]$ignoreIPs = '127.0.0.*';
#    [System.Collections.Generic.List[string]]$ignoreWorkstations;
#    [System.Collections.Generic.List[string]]$ignoreNames = New-Object 'System.Collections.Generic.List[string]';
#    $ignoreNames.Add('OpenDNS_Connector');
    #error checking?

    #This is for ensuring that the auditing logs are set up properly in the first place
    #- If you see repeated errors in this section, there is 99% chance that a group policy is overwriting what you've done here.
    try{
        if(!$ignoreAuditpol){
            $auditCheck = auditpol /get /category:'Logon/logoff';
            $auditOutput = '';
            $tmpInt = 0;
            foreach($auditResult in $auditCheck){
                if($auditResult.length -gt 0){
                    if(!$auditResult.Contains('Success and Failure')){
                        if(!$auditResult.Contains('System audit policy') -and !$auditResult.Contains('Category/Subcategory') -and !$auditResult.Contains('Logon/Logoff')){
                            if($tmpInt -eq 0){
                                Write-Output \"- These fields in the audit log are not set to Success and Failure:\";
                                $tmpInt++;
                            }
                            $auditOutput = '';
                            $aS = $auditResult.Split(' ');
                            foreach($aC in $aS){
                                if($aC.length -gt 0){
                                    $auditOutput += '{0} ' -f $aC;
                                }
                            }
                            Write-Output \"$auditOutput\";
                        }
                    }
                }
            }
            if($tmpInt -gt 0){
                Write-Output \"- Turning on auditing for these fields:\";
                auditpol /set /category:"Logon/logoff" /success:enable /failure:enable;
            }
        }
        else{
            #Auditpol ignored
            #this should only be used in cases where there is an override with Group Policy occurring that is reseting the auditpol
            #it should not be used long term as you cannot guarantee that the event are in the logs and actually avaliable to check.
            auditpol /set /category:"Logon/logoff" /success:enable /failure:enable | Out-Null;
        }
    }
    catch{
        Write-Output 'Error: Failed on auditing logs';
    }

    #Failed Login Check
    try {
        #Setting conditions for event log query
        $checkLogName = 'Security';
        $checkEventID = 4625;
        $checkTime = ([DateTime]::Now.addMinutes(-1*$minutes));
        $eventLog = Get-EventLog -LogName $checkLogName -InstanceId $checkEventID -After $checkTime -ErrorAction SilentlyContinue;
        
        $testLog = $eventLog;
        #applying log to list to make cleaning up easier
        [System.Collections.Generic.List[System.ComponentModel.Component]] $list = $eventLog;

        $ignoreName = 'OpenDNS_Connector';
        $tmpi = 0;
        

        for($i = 0; $i -lt $list.Count; $i++){
            #[string] $tmpStr = Message-FindIP -message $list[$i].Message;
            #IP
            [string] $tmpStr = $list[$i].ReplacementStrings[19];
            for($j = 0; $j -lt $ignoreIPs.Count; $j++){
                if($ignoreIPs[$j].Contains('*')){
                    if(Compare-WildcardString -wildcard $ignoreIPs[$j] -other $tmpStr){
                        $list.RemoveAt($i);
                        $i--;
                    }
                }
                elseif($tmpStr -eq $ignoreIPs[$j]){
                    $list.RemoveAt($i);
                    $i--;
                }
            }

            #Maybe add more ignore cases here

        }
        
        #Kill if below threshold
        if($list.count -lt $checkThreshold){
            Write-Output 'PASS';
            return;
        }

        elseif($list.count -ge $checkThreshold){
            Write-Output \"Total Error Count: $($list.count)\";
            
            #Collecting all the times by minutes and counting the infractions
            $a = @{};
            foreach ($event in $list){
                $time = $event.TimeGenerated.ToString('hh:mm');
                if($a.ContainsKey($time)){
                    $a.$time++;
                }
                else{
                    $a.Add($time, 1);
                }
            }

            #Cant $a | sort hashtables directly, have to sort the objects in it instead
                #https://devblogs.microsoft.com/scripting/weekend-scripter-sorting-powershell-hash-tables/
            $a = $a.GetEnumerator() | Sort-Object -Property Name;

            for($i = 0; $i -lt $a.Length; $i++){
                Write-Output \"$($a[$i].Name) - $($a[$i].Value) failures\";
            }
            Write-Output '';

            #Counting IPs
            $ipCount = ($list | Group-Object {$_.ReplacementStrings[19]}).length;
            if($ipCount -eq $null){
                $ipCount = 1;
            }
            Write-Output \"Unique IPs Attempting to Access: $ipCount\";
            
            $group = $list | Group-Object {$_.Message};
            if($grpLen -eq $null){
                $grpLen = 1;
            }
            for($i=0;$i -lt $grplen; $i++){
                if($group.length -eq $null){
                    $rs = $group.group[0] | Select -expand replacementStrings;
                }
                else{
                    $rs = $group[$i].group[0] | Select -expand replacementStrings;
                }
                $err = switch($rs[8]){
                    '%%2305'{ Write-Output \"The specified user account has expired.\"}
                    '%%2309'{ Write-Output \"The specified accounts password has expired.\"}
                    '%%2310'{ Write-Output \"Account currently disabled.\"}
                    '%%2311'{ Write-Output \"Account logon time restriction violation.\"}
                    '%%2312'{ Write-Output \"User not allowed to logon at this computer.\"}
                    '%%2313'{ Write-Output \"Unknown user name or bad password.\"}
                    default { Write-Output \"Unknown\"}
                    };
                if($group.length -eq $null){
                    Write-Output \"$($group.Count)x: $($rs[19]) : $($rs[20]) - $err\";
                }
                else{
                    Write-Output \"$($group[$i].Count)x: $($rs[19]) : $($rs[20]) - $err\";
                }
                Write-Output \"Logon Type: $($rs[10])\";
                Write-Output \"Logon Account Name Failed: $($rs[5])\";
                Write-Output \"Logon Process: $($rs[12])\";
            }

            if($ignoreIPs.Count -gt 0){
                Write-Output '';
                Write-Output 'Ignored IPs:';
                foreach($ignore in $ignoreIPs){
                    $tempIgnore = '- {0}' -f $ignore;
                    Write-Output $tempIgnore;
                }
            }

        }
    }
    catch {
        Write-Output 'Error: Failed on checking event log';
        if($showErrors){
            Write-Output \"-- Show Errors on querying the eventlog --\";
            for($i =0; $i -lt $Error.Count; $i++){
                Write-Output \"[$i]: $($Error[$i])\";
            }
        }
    }



}




#Need for LT remote monitor
#}"


function Message-FindIP{
    param(
        [Parameter(Mandatory=$true)]
        [string]$message
    )
    $findIP = 'Source Network Address:';
    $ipIndex = $message.IndexOf($findIP)+$findIP.Length;
    $findIPEnd = 'Source Port:';
    $ipEnd = $message.IndexOf($findIPEnd);
    $ip = $message.Substring($ipIndex, $ipEnd-$ipIndex).trim();
    return $ip;
}

#Copied from BitBucket Compare-WildcardString.ps1
#Please edit there and then copy over if you need to make changes
function Compare-WildcardString{
    <#
        .Synopsis
            Compares a string with a wildcard to another string.
        .DESCRIPTION
            Compares 2 strings to see if they match given a wildcard.
            Does not currently support normal string == string comparison.
    #>
    param(
        [Parameter(Mandatory=$true)]
        [string]$wildcard,
        [Parameter(Mandatory=$true)]
        [string]$other,
        [char]$wildcardcharacter = '*'
    )

    if($wildcard.Contains($wildcardcharacter)){
        for($i = 0; $i -lt $wildcard.Length; $i++){
            if($wildcard[$i] -eq $wildcardcharacter){
                return $true;
            }
            elseif($wildcard[$i] -ne $other[$i]){
                return $false;
            }
            #else{
            #    $wildcard[$i];
            #}
        }
    }
    else {
        Write-Error \"String '$wildcard' does not have a wildcard. The default wildcharacter is '*'. The current wildcard is '$wildcardcharacter'\";
        return;
    }
}

function Message-FindWorkstation{
    param(
        [Parameter(Mandatory=$true)]
        [string]$message
    )
    $findWorkstation = 'Workstation Name:';
    $workstationIndex = $message.IndexOf($findWorkstation)+$findWorkstation.Length;
    $findWorkstationEnd = 'Source Network Address:';
    $workstationEnd = $message.IndexOf($findWorkstationEnd);
    $workstation = $message.Substring($workstationIndex, $workstationEnd-$workstationIndex).trim();
    return $workstation;
}

